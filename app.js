const express = require('express')
const app = express()
const port = 8080

app.get('/', (req, res) => {
  res.send('This is my Node application for CI/CD Demo!')
})

app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})